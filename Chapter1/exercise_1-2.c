/*****************************************************************************
 #############################################################################
 ##########  #########   ###############################           ###########
 #########  ########   ############   #################  ##########   ########
 ########  #######   ##########   ####   #############  ############   #######
 #######  ######   ###########   ####  ##############  ##########   ##########
 ######         ################   #######    ######            ##############
 #####  #######  ############  ###   ###   ########  ##########  #############
 ####  #########  #########   #####   #  #########  ############  ############
 ###  ###########  #######   #######   ##########  ##############  ###########
 ##  #############  ########   ###  #   ########  ################  ##########
 #  ###############  ##########  ######    ####  ##################  #########
 #############################################################################
 #############################################################################
 ########        ########     #######    #  ##  #    ###    ####       #######
 ######   #############  ####  ###  ####   ##    ##  #  ##  ##   ####  #######
 #####  #######     ###  ##   ##  ######  ##  #####   ###  ##   ##############
 ######   #################  ####   ###  ##  ###########  ####  ##############
 ########     #####   ####  #########   ##  ###########  ######     ##########
 ###################       ###################################################
 #############################################################################
 =============================================================================
 | This code is part of the K&R group game, where the goal is to solve every |
 | exercise of the book in the most elegant and efficient way. The K&R C book|
 | can be consulted on the Telegram group. More information about the game   |
 | is provided in the README.md file located at the master branch.           |
 |----------------------------------------------------------------------------
 |                                                                           |
 |          Coded by izxle                                                   |
 |          Date:    2021-02-10                                              |
 |                                                                           |
 |----------------------------------------------------------------------------
 |                                                                           |
 | Exercise 1-1. Experiment to find out what happens when `printf`'s argument|
 | string contains \c, where c is some character not listed above.           |
 |                                                                           |
 |----------------------------------------------------------------------------
 |                                                                           |
 | Explanation (in cases needed):                                            |
 | Unknown escaped characters are printed by `printf` as normal characters,  |
 | the backslash is ignored.                                                 |
 =============================================================================
 *****************************************************************************/
#include <stdio.h>

int main()
{
    printf("\dhello, ");
    printf("world");
    printf("\n\c");
}